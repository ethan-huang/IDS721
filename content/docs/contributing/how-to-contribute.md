+++
title = "Research"
description = "Contribute to AdiDoks, improve documentation, or submit to showcase."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Porsche Panamera"
toc = true
top = false
+++

<img src = "../car.jpeg" width = 50%> <br> <br> <br>
This car's picture is formed by thousands of mini-pictures in my cell phone. Each pixel is replaced with a mini-picture by the use of C++ and KD-tree.
