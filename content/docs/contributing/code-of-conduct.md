+++
title = "Machine Projects"
description = "Contributor Covenant Code of Conduct."
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = ""
toc = true
top = false
+++

### Airbnb Price Prediction

1. Developed machine learning techniques to predict the price of various types of Airbnb based on 1000+ features.
2. Employed the property of loss functions to find the upper bound of useful features and improved efficiency on splitting tree nodes.
3. Reduced the number of features from 1000 to 20 to save the running time of random forest and XGB model by using out-of-bag error to rank each variable’s importance and selected the most 20 important features related to price.
4. Used feature engineering to fill missing data with majority values, one-hot encoding for categorical features, target encoding for categorical features over 4 classes to train tree-based models.
5. Ensembled various trained models to capture different characteristics from training data: random forest, XGB, and logistic regression.

### Algorithm for Ambulance Dispatch Research

1. Developed a dynamic programming algorithm combined with Bayesian inference method to dispatch ambulances.
2. ptimized dispatching functions by relaxing the constraints of the loss functions and utilizing duality to find optimal integer solutions to dispatch ambulances for different locations.
3. Achieved simulation of 200+ ambulances dispatching on time and avoid short-handed situations.
