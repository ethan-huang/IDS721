+++
title = "Introduction"
description = "AdiDoks is a Zola theme helping you build modern documentation websites, which is a port of the Hugo theme Doks for Zola."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '<img src = "../IMG_8414.jpg" width = 50%>'

toc = true
top = false
+++

Hello, my name is Ethan Huang. I did my undergrad at University of Illinois Urbana-Champaign. I am currently in the MS-CS program at Duke.

## Skills

Python, C++, Java, HTML..(to be continued)

## Courses

Advanced machine learning, Data Science, Mobile App, Advanced Algorithms..(to be continued)
